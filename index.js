// Basic
const fs = require('fs');
const path = require('path');

// General
const minimist = require('minimist');

// Custom modules
const htmlProcessor = require('./html_processor');

// Command-line args
var args = minimist(process.argv.slice(2));

// Print help
if (args.help) {
    // List argument possibilities etc.
    console.log('--html    To specify input html file');

    process.exit();
}

(async () => {
    var proc = new htmlProcessor(
        path.resolve(__dirname, args.html || './test_files/test.html')
    );

    try {
        var html = await proc.process();

        try {
            // Write out the file
            fs.writeFileSync(proc.file.replace(/\.html$/, '') + '-out.html', html);
        } catch (err) {
            console.error('Failed to output to a target file: %s', err);
        }
    } catch (err) {
        console.error('Processing failed: %s', err.message);
    }
})();
