const fs = require('fs');
const path = require('path');

const cheerio = require('cheerio');

const cssProcessor = require('./css_processor');

function loadFile(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, (err, contents) => {
            if (err)
                return reject(err);

            resolve(contents.toString());
        })
    });
}

class HtmlProcessor {
    constructor(file) {
        this.file = file;
    }

    async process() {
        // Get file contents
        let fileContents = await loadFile(this.file);

        // Get CSS link tag and remove it from the html
        let cssLinkTag = null;
        fileContents = fileContents.replace(/\s+<link([^>]+)>/, (orig) => {
            // Extract the link tag attributes
            cssLinkTag = cheerio.load(orig)('link').attr();

            return '';
        });

        if (!cssLinkTag)
            throw new Error('No <link> tag found!');

        if (!cssLinkTag.href)
            throw new Error('<link> tag does not have a href attribute!');

        if (cssLinkTag.href.indexOf('/') == 0)
            throw new Error('CSS href attribute starts with slash!');

        // Get the full path to the css file
        let cssFilePath = path.resolve(path.dirname(this.file), cssLinkTag.href);

        // Load up css rules
        let proc = new cssProcessor(cssFilePath);
        return await proc.process(fileContents);
    }
}

module.exports = HtmlProcessor;