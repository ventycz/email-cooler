const fs = require('fs');

const parseCSS = require('css-rules');
const cheerio = require('cheerio');

function loadFile(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, (err, contents) => {
            if (err)
                return reject(err);

            resolve(contents.toString());
        })
    });
}

class CssProcessor {
    constructor(file) {
        this.file = file;
    }

    async process(htmlContents) {
        // Get css file contents
        var fileContents = await loadFile(this.file);

        // Parse html with cheerio
        const $ = cheerio.load(htmlContents);

        // Parse the css file
        var rules = parseCSS(fileContents);

        // Apply all the rules
        rules.forEach((rule) => {
            // Get target element
            let element = $(rule[0]);

            // Element not found, skip!
            if (!element.length)
                return;

            /**
             * @type CSSStyleDeclaration
             */
            let styles = rule[1];

            // Apply each property to the target element
            for (var i = 0; i < styles.length; i++) {
                var prop = styles[i];
                var val = styles[prop];

                element.css(prop, val);
            }
        });

        return $.html({ decodeEntities: false });
    }
}

module.exports = CssProcessor;